apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-myapp
  labels:
    app: myapp
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.myapp.replicas }}
  revisionHistoryLimit: {{ .Values.myapp.revisionHistoryLimit }}
  selector:
    matchLabels:
      app: myapp
    {{- include "meu-chart-helmify.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app: myapp
      {{- include "meu-chart-helmify.selectorLabels" . | nindent 8 }}
    spec:
      containers:
      - args: {{- toYaml .Values.myapp.app.args | nindent 8 }}
        command:
        - /manager
        env:
        - name: VAR1
          valueFrom:
            secretKeyRef:
              key: VAR1
              name: {{ include "meu-chart-helmify.fullname" . }}-my-secret-vars
        - name: VAR2
          valueFrom:
            secretKeyRef:
              key: VAR2
              name: {{ include "meu-chart-helmify.fullname" . }}-my-secret-vars
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.myapp.app.image.repository }}:{{ .Values.myapp.app.image.tag
          | default .Chart.AppVersion }}
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        name: app
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        resources: {{- toYaml .Values.myapp.app.resources | nindent 10 }}
        securityContext: {{- toYaml .Values.myapp.app.containerSecurityContext | nindent
          10 }}
        volumeMounts:
        - mountPath: /my_config.properties
          name: manager-config
          subPath: my_config.properties
        - mountPath: /my.ca
          name: secret-volume
        - mountPath: /etc/props
          name: props
        - mountPath: /usr/share/nginx/html
          name: sample-pv-storage
      - args: {{- toYaml .Values.myapp.proxySidecar.args | nindent 8 }}
        env:
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.myapp.proxySidecar.image.repository }}:{{ .Values.myapp.proxySidecar.image.tag
          | default .Chart.AppVersion }}
        name: proxy-sidecar
        ports:
        - containerPort: 8443
          name: https
        resources: {}
      initContainers:
      - command:
        - /bin/sh
        - -c
        - echo Initializing container...
        env:
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.myapp.initContainer.image.repository }}:{{ .Values.myapp.initContainer.image.tag
          | default .Chart.AppVersion }}
        name: init-container
        resources: {}
      nodeSelector: {{- toYaml .Values.myapp.nodeSelector | nindent 8 }}
      securityContext:
        runAsNonRoot: true
      terminationGracePeriodSeconds: 10
      volumes:
      - configMap:
          name: {{ include "meu-chart-helmify.fullname" . }}-my-config
        name: manager-config
      - configMap:
          name: {{ include "meu-chart-helmify.fullname" . }}-my-config-props
        name: props
      - name: secret-volume
        secret:
          secretName: {{ include "meu-chart-helmify.fullname" . }}-my-secret-ca
      - name: sample-pv-storage
        persistentVolumeClaim:
          claimName: {{ include "meu-chart-helmify.fullname" . }}-my-sample-pv-claim
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-my-sample-pv-claim
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    limits:
      storage: {{ .Values.pvc.mySamplePvClaim.storageLimit | quote }}
    requests:
      storage: {{ .Values.pvc.mySamplePvClaim.storageRequest | quote }}
  storageClassName: {{ .Values.pvc.mySamplePvClaim.storageClass | quote }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-myapp-service
  labels:
    app: myapp
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  type: {{ .Values.myappService.type }}
  selector:
    app: myapp
  {{- include "meu-chart-helmify.selectorLabels" . | nindent 4 }}
  ports:
	{{- .Values.myappService.ports | toYaml | nindent 2 -}}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-myapp-ingress
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - backend:
          service:
            name: '{{ include "meu-chart-helmify.fullname" . }}-myapp-service'
            port:
              number: 8443
        path: /testpath
        pathType: Prefix
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-my-secret-ca
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
data:
  ca.crt: {{ required "mySecretCa.caCrt is required" .Values.mySecretCa.caCrt | b64enc
    | quote }}
type: opaque
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-my-secret-vars
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
data:
  ELASTIC_FOOBAR_HUNTER123_MEOWTOWN_VERIFY: {{ required "mySecretVars.elasticFoobarHunter123MeowtownVerify is required" .Values.mySecretVars.elasticFoobarHunter123MeowtownVerify | b64enc
    | quote }}
  VAR1: {{ required "mySecretVars.var1 is required" .Values.mySecretVars.var1 | b64enc
    | quote }}
  VAR2: {{ required "mySecretVars.var2 is required" .Values.mySecretVars.var2 | b64enc
    | quote }}
stringData:
  str: {{ required "mySecretVars.str is required" .Values.mySecretVars.str | quote
    }}
type: opaque
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-my-config
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
immutable: true
data:
  dummyconfigmapkey: {{ .Values.myConfig.dummyconfigmapkey | quote }}
  my_config.properties: |
    health.healthProbeBindAddress={{ .Values.myConfig.myConfigProperties.health.healthProbeBindAddress | quote }}
    metrics.bindAddress={{ .Values.myConfig.myConfigProperties.metrics.bindAddress | quote }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-my-config-props
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
data:
  my.prop1: {{ .Values.myConfigProps.myProp1 | quote }}
  my.prop2: {{ .Values.myConfigProps.myProp2 | quote }}
  my.prop3: {{ .Values.myConfigProps.myProp3 | quote }}
  myval.yaml: {{ .Values.myConfigProps.myvalYaml | toYaml | indent 1 }}
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-fluentd-elasticsearch
  labels:
    k8s-app: fluentd-logging
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
    {{- include "meu-chart-helmify.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
      {{- include "meu-chart-helmify.selectorLabels" . | nindent 8 }}
    spec:
      containers:
      - env:
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.fluentdElasticsearch.fluentdElasticsearch.image.repository }}:{{
          .Values.fluentdElasticsearch.fluentdElasticsearch.image.tag | default .Chart.AppVersion
          }}
        name: fluentd-elasticsearch
        resources: {{- toYaml .Values.fluentdElasticsearch.fluentdElasticsearch.resources
          | nindent 10 }}
        volumeMounts:
        - mountPath: /var/log
          name: varlog
        - mountPath: /var/lib/docker/containers
          name: varlibdockercontainers
          readOnly: true
      terminationGracePeriodSeconds: 30
      tolerations:
      - effect: NoSchedule
        key: node-role.kubernetes.io/master
        operator: Exists
      volumes:
      - hostPath:
          path: /var/log
        name: varlog
      - hostPath:
          path: /var/lib/docker/containers
        name: varlibdockercontainers
---
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-batch-job
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  backoffLimit: {{ .Values.batchJob.backoffLimit }}
  template:
    spec:
      containers:
      - command:
        - perl
        - -Mbignum=bpi
        - -wle
        - print bpi(2000)
        env:
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.batchJob.pi.image.repository }}:{{ .Values.batchJob.pi.image.tag
          | default .Chart.AppVersion }}
        name: pi
        resources: {}
      restartPolicy: Never
---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-cron-job
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - command:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
            env:
            - name: KUBERNETES_CLUSTER_DOMAIN
              value: {{ quote .Values.kubernetesClusterDomain }}
            image: {{ .Values.cronJob.hello.image.repository }}:{{ .Values.cronJob.hello.image.tag
              | default .Chart.AppVersion }}
            imagePullPolicy: {{ .Values.cronJob.hello.imagePullPolicy }}
            name: hello
            resources: {}
          restartPolicy: OnFailure
  schedule: {{ .Values.cronJob.schedule | quote }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-nginx
  labels:
    app: nginx
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  type: {{ .Values.nginx.type }}
  selector:
    app: nginx
  {{- include "meu-chart-helmify.selectorLabels" . | nindent 4 }}
  ports:
	{{- .Values.nginx.ports | toYaml | nindent 2 -}}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-web
  labels:
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.web.replicas }}
  selector:
    matchLabels:
      app: nginx
  serviceName: {{ include "meu-chart-helmify.fullname" . }}-nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - env:
        - name: KUBERNETES_CLUSTER_DOMAIN
          value: {{ quote .Values.kubernetesClusterDomain }}
        image: {{ .Values.web.nginx.image.repository }}:{{ .Values.web.nginx.image.tag
          | default .Chart.AppVersion }}
        name: nginx
        ports:
        - containerPort: 80
          name: web
        resources: {}
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: www
  updateStrategy: {}
  volumeClaimTemplates:
  - metadata:
      creationTimestamp: null
      name: www
    spec:
      accessModes:
      - ReadWriteOnce
      resources: {{ .Values.web.volumeClaims.www | toYaml | nindent 8 }}
---
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: {{ include "meu-chart-helmify.fullname" . }}-myapp-pdb
  labels:
    app: nginx
  {{- include "meu-chart-helmify.labels" . | nindent 4 }}
spec:
  minAvailable: {{ .Values.myappPdb.minAvailable }}
  maxUnavailable: {{ .Values.myappPdb.maxUnavailable }}
  selector:
    matchLabels:
      app: nginx
    {{- include "meu-chart-helmify.selectorLabels" . | nindent 6 }}