# Study Helm

<img src="./pics/helm.png" alt="drawing" width="200"/>

Esse repositório foca no estudo do gereciador de pacote do kubernetes [Helm](https://helm.sh/)

[Documentação oficial do Helm](https://helm.sh/docs/)

Assim como toda linguagem de programação, o kubernetes também possue seu gerenciador de pacotes que é Helm.

Helm faz parta da [CNCF](https://www.cncf.io/projects/helm/) e é um projeto graduado baseado em golang.

Antes de começar vamos esquecer a o helm versão 2 e forcar só na versão 3 que é a mais nova.

## Livro

[Conceitos Básicos](./manuscritos/1%20Conceitos.md)

[instalando o Helm](./manuscritos/2%20Instala%C3%A7%C3%A3o.md)

[instalando um chart](./manuscritos//3%20Instalando%20Chart%20com%20o%20Helm.md)

[Criando um chart](./manuscritos/1%20Conceitos.md)

## Instalando um chart

<https://helm.sh/docs/helm/helm_install/>

Vamos olhar os charts disponíveis em <https://artifacthub.io/>

Fiz uma pesquisa por [prometheus](https://artifacthub.io/packages/helm/prometheus-community/prometheus) e ele mesmo já mostra todo o caminho que devemos fazer na documentação do chart.

a sintaxe de instalação é `helm install [NAME] [CHART] [flags]`

````bash
❯ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

"prometheus-community" has been added to your repositories

❯ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "prometheus-community" chart repository
Update Complete. ⎈Happy Helming!⎈
````

Observe que na hora de instalar precisamos passar o nome da release. Essa release é a versão do chart e não do prometheus.

Como vamos instalar o prometheus, eu gosto se separar os namespaces por tipos de ferramentas.

````bash
❯ k create namespace observability
namespace/observability created

## Se vc não passar uma versão ele irá pegar a ultima
# Os dois comandos set foi pra ele não criar o pod específico, não se atente a isso.
# Leia com atenção a saída
❯ helm install prometheus prometheus-community/prometheus --namespace=observability  --set alertmanager.persistentVolume.enabled=false --set server.persistentVolume.enabled=false
NAME: prometheus
LAST DEPLOYED: Thu Sep 22 15:37:06 2022
NAMESPACE: observability
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-server.observability.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace observability -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace observability port-forward $POD_NAME 9090
#################################################################################
######   WARNING: Persistence is disabled!!! You will lose your data when   #####
######            the Server pod is terminated.                             #####
#################################################################################


The Prometheus alertmanager can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-alertmanager.observability.svc.cluster.local


Get the Alertmanager URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace observability -l "app=prometheus,component=alertmanager" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace observability port-forward $POD_NAME 9093
#################################################################################
######   WARNING: Persistence is disabled!!! You will lose your data when   #####
######            the AlertManager pod is terminated.                       #####
#################################################################################
#################################################################################
######   WARNING: Pod Security Policy has been moved to a global property.  #####
######            use .Values.podSecurityPolicy.enabled with pod-based      #####
######            annotations                                               #####
######            (e.g. .Values.nodeExporter.podSecurityPolicy.annotations) #####
#################################################################################


The Prometheus PushGateway can be accessed via port 9091 on the following DNS name from within your cluster:
prometheus-pushgateway.observability.svc.cluster.local


Get the PushGateway URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace observability -l "app=prometheus,component=pushgateway" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace observability port-forward $POD_NAME 9091

For more information on running Prometheus, visit:
https://prometheus.io/
````

o comando `helm status prometheus` mostra a mesma saída da instalção.

confira alguns recursos para ver que aconteceu de fato

````bash
❯ k get pods -n observability
NAME                                             READY   STATUS    RESTARTS   AGE
prometheus-alertmanager-5b4978459b-jnwcm         2/2     Running   0          45s
prometheus-kube-state-metrics-6d85d4d94b-d7jbd   1/1     Running   0          45s
prometheus-node-exporter-mtqcs                   1/1     Running   0          45s
prometheus-node-exporter-xgfhn                   1/1     Running   0          45s
prometheus-pushgateway-c99895f4c-fhg2m           1/1     Running   0          45s
prometheus-server-7dc9f67ccb-s6lv6               2/2     Running   0          45s

❯ k get svc -n observability
NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
prometheus-alertmanager         ClusterIP   172.20.17.255    <none>        80/TCP     101s
prometheus-kube-state-metrics   ClusterIP   172.20.171.87    <none>        8080/TCP   101s
prometheus-node-exporter        ClusterIP   172.20.196.225   <none>        9100/TCP   101s
prometheus-pushgateway          ClusterIP   172.20.254.164   <none>        9091/TCP   101s
prometheus-server               ClusterIP   172.20.16.117    <none>        80/TCP     101s

❯ k get deploy -n observability
NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
prometheus-alertmanager         1/1     1            1           117s
prometheus-kube-state-metrics   1/1     1            1           117s
prometheus-pushgateway          1/1     1            1           117s
prometheus-server               1/1     1            1           117s
````

Uma coisa interessante a se analisar é que ele criou tudo com clusterip, logo só é visivel dentro do cluster. Se vc editar dentro de prometheus-server de clusterIP para loadbalancer ou nodeport terá um acesso externo.

Agora para vc ver o que vc tem no seu helm criado

````bash
❯ helm list
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART              APP VERSION
prometheus      observability   1               2022-09-22 15:10:01.154700571 -0300 -03 deployed        prometheus-15.13.0 2.36.2     

❯ helm status prometheus
````

Vamos instalar o grafana também.

````bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

helm install grafana grafana/grafana --set adminUser=admin --set adminPassword=admin --set service.type=LoadBalancer

❯ k get pods -n observability
NAME                                             READY   STATUS    RESTARTS   AGE
grafana-79bb6b8cd8-vcjnn                         1/1     Running   0          13s
prometheus-alertmanager-5b4978459b-jnwcm         2/2     Running   0          11m
prometheus-kube-state-metrics-6d85d4d94b-d7jbd   1/1     Running   0          11m
prometheus-node-exporter-mtqcs                   1/1     Running   0          11m
prometheus-node-exporter-xgfhn                   1/1     Running   0          11m
prometheus-pushgateway-c99895f4c-fhg2m           1/1     Running   0          11m
prometheus-server-7dc9f67ccb-s6lv6               2/2     Running   0          11m

❯ k get svc -n observability grafana
NAME      TYPE           CLUSTER-IP      EXTERNAL-IP                                                               PORT(S)        AGE
grafana   LoadBalancer   172.20.55.123   ae7874263f16d4246ad83682ab001189-1638700145.us-east-1.elb.amazonaws.com   80:31844/TCP   63s                                                             80/TCP         11m

❯ helm list
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART              APP VERSION
grafana         observability   1               2022-09-22 15:48:20.938250426 -0300 -03 deployed        grafana-6.38.6     9.1.5      
prometheus      observability   1               2022-09-22 15:37:06.7122254 -0300 -03   deployed        prometheus-15.13.0 2.36.2     
````

Se vc bater nesse host ae7874263f16d4246ad83682ab001189-1638700145.us-east-1.elb.amazonaws.com

## repo

Onde encontrar os repos? <https://artifacthub.io/>

procure o repo da ferramenta que vc quer e veja no describe que já vai te dar qual o repo, além do que vc precisa para instalar.

Manipulando os repos.

````bash
❯ helm repo add traefik https://helm.traefik.io/traefik 
"traefik" has been added to your repositories

❯ helm repo list                                       
NAME                    URL                                               
prometheus-community    https://prometheus-community.github.io/helm-charts
traefik                 https://helm.traefik.io/traefik

❯ helm repo remove traefik 
"traefik" has been removed from your repositories
````

## Search

O comando search faz a mesma coisa que entrar no site e procurar o que deseja, mas de forma mais rápida apesar de que eu prefiro procurar online!

````bash
❯ helm search hub traefik
URL                                                     CHART VERSION   APP VERSION     DESCRIPTION                                       
https://artifacthub.io/packages/helm/traefik/tr...      10.24.3         2.8.5           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/drycc-cana...      10.24.1         2.8.0           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/cloudnativ...      1.68.4          1.7.9           A Traefik based Kubernetes ingress controller w...
https://artifacthub.io/packages/helm/truecharts...      13.3.7          2.8.4           Traefik is a flexible reverse proxy and Ingress...
https://artifacthub.io/packages/helm/kube-ops/t...      1.4.0           2.5.1           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/wener/traefik      9.1.1           2.2.8           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/wenerme/tr...      9.1.1           2.2.8           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/itscontain...      9.18.4          2.4.8           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/banzaiclou...      1.75.1          1.7.12          A Traefik based Kubernetes ingress controller w...
https://artifacthub.io/packages/helm/mesosphere...      1.90.0          1.7.24          A Traefik based Kubernetes ingress controller w...
https://artifacthub.io/packages/helm/aigisuk/tr...      0.1.1           2.7.0-rc2       Latest release candidate of the Traefik based K...
https://artifacthub.io/packages/helm/mmontes/tr...      0.1.0           2.4.8           Traefik wrapper by mmontes11                      
https://artifacthub.io/packages/helm/traefik-me...      4.0.4           v1.4.8          Traefik Mesh - Simpler Service Mesh               
https://artifacthub.io/packages/helm/pascaliske...      2.2.3           1.0.4           A Helm chart for custom traefik error pages       
https://artifacthub.io/packages/helm/wiremind/t...      2.6.3           2.6.3           Manage traefik CRDs                               
https://artifacthub.io/packages/helm/mesosphere...      0.3.8           3.1.0           Minimal forward authentication service that pro...
https://artifacthub.io/packages/helm/itscontain...      1.0.2           2.2.0           A minimal forward authentication service that p...
https://artifacthub.io/packages/helm/mesosphere...      0.0.1           0.0.1           A Helm chart for Kubernetes that exposes thanos...
https://artifacthub.io/packages/helm/traefik-jw...      0.1.0                           A Helm chart for Kubernetes                       
https://artifacthub.io/packages/helm/kubitodev/...      1.0.4           2.8.0           Kubito Traefik Whitelist DDNS Helm Chart          
https://artifacthub.io/packages/helm/kubitodev/...      1.0.4           2.8.0           Kubito Traefik Cloudflared (Argo Tunnel) Real I...
https://artifacthub.io/packages/helm/philippwal...      1.0.2           v1.0.1          This Helm Chart allows to persistently overwrit...
https://artifacthub.io/packages/helm/platydev/t...      0.1.9           1.0             Traefik pre-configured with cert-manager and Le...
https://artifacthub.io/packages/helm/colearendt...      0.0.10                          Deploy traefik-forward-auth                       
https://artifacthub.io/packages/helm/crowdsec/c...      0.1.1           0.3.5           A http service to verify request and bounce the...
https://artifacthub.io/packages/helm/traefik-me...      2.1.2           v1.3.2          Maesh - Simpler Service Mesh                      
https://artifacthub.io/packages/helm/mesosphere...      9.18.0          2.4.8           A Traefik based Kubernetes ingress controller     
https://artifacthub.io/packages/helm/cryptexlab...      0.7.1           0.7.1           Forward authentication service                    
https://artifacthub.io/packages/helm/kiwigrid/e...      0.1.2           1.0             A Helm chart for Kubernetes error pages for tra...
https://artifacthub.io/packages/helm/samipsolut...      1.2.2           2.6.0           Server error pages in the docker image            
https://artifacthub.io/packages/helm/flagger/fl...      1.22.2          1.22.2          Flagger is a progressive delivery operator for ...
https://artifacthub.io/packages/helm/dniel/forw...      2.0.13          2.0-rc1         A Helm chart for Kubernetes to install Auth0 Au...
https://artifacthub.io/packages/helm/mesosphere...      0.1.1           1.0             Adds an traefik ingress record using the provid...
https://artifacthub.io/packages/helm/taskmedia/...      1.1.4           latest          Deploy IPsec VPN server inside K8s with optiona...
https://artifacthub.io/packages/helm/wenerme/me...      0.6.8           v0.6.8          Meshery chart for deploying Meshery and Meshery...
https://artifacthub.io/packages/helm/wener/meshery      0.6.8           v0.6.8          Meshery chart for deploying Meshery and Meshery...
https://artifacthub.io/packages/helm/meshery/me...      0.6.8           v0.6.8          Meshery chart for deploying Meshery and Meshery...
https://artifacthub.io/packages/helm/riotkit-or...      1.0.6           v1.0.6          Simple TLS-SNI based router using Traefik and N...
https://artifacthub.io/packages/helm/switchboar...      0.5.1           0.5.1                   
````
