# Criando um chart

>Geralmente cria-se um chart depois que se tem todos tudo pronto de uma aplicação, para depois somente parametrizar os valores dinamicamente.

Vamos criar o nosso primeiro chart do zero e depois vamos criar um chart baseado em outro como um subchart.

A primeira coisa que precisamos fazer é a estrutura de diretórios

````bash
mkdir meu-chart && cd meu-chart && mkdir templates charts && touch Chart.yaml values.yaml
tree
.
├── Chart.yaml
├── charts
├── templates
└── values.yaml
````

Com a estrutura criada, vamos definir algumas informações sobre nosso chart dentro do arquivo Chart.yaml.
> Para encontrar todos os parametros que poderiam vir dentro desse arquivo <https://helm.sh/docs/topics/charts/>

Copie para dentro de Chart.yaml

````yaml
apiVersion: v2
name: meuchart
version: v1.0
kubeVersion: >= 1.20.0
description: Create my first chart 
type: application
keywords:
  - study charts
  - first chart
  - learning charts
  - hello world
home: https://gitlab.com/davidpuziol/study-helm/-/tree/main/meu-chart #opcional
maintainers: # (optional)
  - name: David Puziol  
    email: meuemail@gmail.com
    url: https://linktr.ee/davidpuziol
icon: https://cncf-branding.netlify.app/img/projects/helm/icon/color/helm-icon-color.png
appVersion: 'v1.0.0' #opcional
deprecated: false
````

Agora vamos definir nosso templates, que no caso é de fato os nosso manifestos porém referenciando valores dinamicamente.
Crie um arquivo deployment.yaml dentro de templates com o seguinte conteúdo:

Observe que os valores são referenciado dentro de `{{ }}` e temos alguns para analiasar

Para consultar possíveis valores para .Release e .Chart conferir a documentação <https://helm.sh/docs/chart_template_guide/builtin_objects/>

- .Release
  - Referencia valores recebidos na hora da instalação Por exemplo se passar um namespace. É algo que não está no Chart.yaml mas foi adicionado em tempo de execução como parametro. Um exemplo seria o `--namespace meu-release`. Irá pegar com o .Release.namespace.
- .Chart
  - Trabalharam com os metadatas do chart definidos no arquivo Chart.yaml
- .Values
  - Essa variável irá conter as configurações definidas dentro do arquivo Values.yaml (configurações padrões) ou do arquivo passado no parâmetro -f no momento da execução do Helm.
  > Ainda é possível a passagem de valores para os templates através do parâmetro -- set. Sendo estas fazendo uma substituição Values.yaml utilizado.
- .Capabilities
  - Referencia ao cluster kubernetesou ao helm
- .Template
  - Informações sobre o template que está sendo executado. Seria como uma auto referencia.

````yaml
apiVersion: apps/v1beta2
kind: Deployment
metadata:
# Este valor vem de Chart.yaml 
  name: {{ .Release.Name }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ .Release.Name }}
      release: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}
        release: {{ .Release.Name }}
    spec:
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
````

Do mesmo modo, crie em templates um service.yaml com o conteúdo abaixo.

````yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: http
      protocol: TCP
      name: http
  selector:
    app: {{ .Release.Name }}
    release: {{ .Release.Name }}
````

E vamos completar agora o nosso values.

`````yaml
image:
  repository: nginx
  tag: latest
  imagePullPolicy: IfNotPresent
replicaCount: 1
service:
  type: NodePort
  port: 80
`````

Vamos criar

```bash
helm create meu-chart 
```

utilizar o comando `helm upgrade —-install` ao invés do comando `helm install` porque ele já verifica se o pacote está instalado no cluster, caso esteja ele apenas faz a sua atualização.

Se vc quiser ver antes de aplicar o que ele irá gerar sem de fato executar e ver a saída, bem como debugar, pode ser feito

````bash
helm upgrade --install meu-chart . --dry-run --debug > dryrun.yaml
````

````bash
helm upgrade --install meu-chart .
# Release "meu-chart" does not exist. Installing it now.
# NAME: meu-chart
# LAST DEPLOYED: Mon Dec 26 03:19:25 2022
# NAMESPACE: default
# STATUS: deployed
# REVISION: 1
# TEST SUITE: None

 k get pods
# NAME                        READY   STATUS    RESTARTS   AGE
# meu-chart-94b9bf4bb-lk8sp   1/1     Running   0          7s
````

Se quiser testar

````bash
kubectl port-forward svc/meu-chart <nodeport>:80
````

![testnginx](../pics/testnginx.png)
