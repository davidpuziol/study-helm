# Helmify

<https://github.com/arttor/helmify>

Se você já possui alguns manifestos e deseja convertir em um chart essa é a ferramenta que irá ajudar.

Helmify é uma CLI que cria helm charts a partir de manifestos do Kubernetes. Não é perfeito, pois não suporta todos os possíveis objetos que podemso criar com kubernetes, afinal o kubernetes tem uma api extensível. Projetado para gerar gráficos para operadores k8s , mas não limitado.

Os objetos suportados são <https://github.com/arttor/helmify#status>

Baixe o arquivo tar necessário no URL <https://github.com/arttor/helmify/releases> e execute os comandos abaixo.

```bash
tar -xvzf helmify_0.4.3_Linux_64-bit.tar.gz
chmod +x helmify
sudo cp helmify /usr/bin/

helmify --version        
```

Temos poucas opções de como executar, mas funcionais

```bash
helmify --help   

Helmify parses kubernetes resources from std.in and converts it to a Helm chart.

Example 1: 'kustomize build <kustomize_dir> | helmify mychart' 
  - will create 'mychart' directory with Helm chart from kustomize output.

Example 2: 'cat my-app.yaml | helmify mychart' 
  - will create 'mychart' directory with Helm chart from yaml file.

Example 3: 'helmify -f ./test_data/dir  mychart' 
  - will scan directory ./test_data/dir for files with k8s manifests and create 'mychart' directory with Helm chart.

Example 4: 'helmify -f ./test_data/dir -r  mychart' 
  - will scan directory ./test_data/dir recursively and  create 'mychart' directory with Helm chart.

Example 5: 'helmify -f ./test_data/dir -f ./test_data/sample-app.yaml -f ./test_data/dir/another_dir  mychart' 
  - will scan provided multiple files and directories and  create 'mychart' directory with Helm chart.

Example 6: 'awk 'FNR==1 && NR!=1  {print "---"}{print}' /my_directory/*.yaml | helmify mychart' 
  - will create 'mychart' directory with Helm chart from all yaml files in my_directory directory.

Usage:
  helmify [flags] CHART_NAME  -  CHART_NAME is optional. Default is 'chart'. Can be a directory, e.g. 'deploy/charts/mychart'.

Flags:
  -cert-manager-as-subchart
        Allows the user to add cert-manager as a subchart
  -cert-manager-version string
        Allows the user to specify cert-manager subchart version. Only useful with cert-manager-as-subchart. (default "v1.12.2")
  -crd-dir
        Enable crd install into 'crds' directory.
        Warning: CRDs placed in 'crds' directory will not be templated by Helm.
        See https://helm.sh/docs/chart_best_practices/custom_resource_definitions/#some-caveats-and-explanations
        Example: helmify -crd-dir
  -f value
        File or directory containing k8s manifests
  -generate-defaults
        Allows the user to add empty placeholders for tipical customization options in values.yaml. Currently covers: topology constraints, node selectors, tolerances
  -h    Print help. Example: helmify -h
  -help
        Print help. Example: helmify -help
  -image-pull-secrets
        Allows the user to use existing secrets as imagePullSecrets in values.yaml
  -r    Scan dirs from -f option recursively
  -v    Enable verbose output (print WARN & INFO). Example: helmify -v
  -version
        Print helmify version. Example: helmify -version
  -vv
        Enable very verbose output. Same as verbose but with DEBUG. Example: helmify -vv
```

Vamos criar um chart par ao [sample-app.yaml](../resources/sample-app.yaml)

```bash
helmify -f ./resources/sample-app.yaml meu-chart-helmify 
tree meu-chart-helmify   

meu-chart-helmify
├── Chart.yaml
├── templates
│   ├── _helpers.tpl
│   └── sample-app.yaml
└── values.yaml
```
