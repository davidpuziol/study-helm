# Plugins

Algusn plugins podem ser usados junto ao comando helm.

## Helm Dashboard

<https://github.com/komodorio/helm-dashboard>

Helm Dashboard é um projeto de código aberto que oferece uma maneira orientada pela UI de visualizar os gráficos Helm instalados, ver seu histórico de revisões e recursos k8s correspondentes. Ele também permite que os usuários executem ações simples, como reverter para uma revisão ou atualizar para uma versão mais recente.

```bash
### Para instalar
helm plugin install https://github.com/komodorio/helm-dashboard.git
## O comando abaixo irá rodar o dashboard
helm dashboard
## Para desinstalar
helm plugin uninstall dashboard                                    
## 
```

## Helm Secrets

helm-secrets é um plugin de helm para descriptografar arquivos de valor de helm criptografados em tempo real.

```bash
### Para instalar em uma versão específica. Se nao passar a verao instalará a latest
helm plugin install https://github.com/jkroepke/helm-secrets
### Esse comando abaixo iria instalar em uma versão específica
helm plugin install https://github.com/jkroepke/helm-secrets --version v4.5.1

## O comando abaixo irá rodar o dashboard
helm dashboard
## Para desinstalar
helm plugin uninstall dashboard                                    
## 
```
